export default (maxLength, string) => {
    string = string.toString();

    if (string.length <= maxLength) {
        return string;
    }

    const endPosition = string.slice(0, maxLength).lastIndexOf(' ');

    return string.slice(0, endPosition) + ' [...]'
}