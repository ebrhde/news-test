import './App.css';
import 'semantic-ui-css/semantic.min.css';

import Index from "./pages/Index";
import Menu from "./components/Menu";

function App() {
  return (
    <div className="App">
        <Menu />
        <Index />
    </div>
  );
}

export default App;
