import Header from "../components/Header";
import {Button, Container, Divider, Modal} from "semantic-ui-react";
import {useState} from "react";
import Table from "../components/Table";
import NewsForm from "../components/Form";
import axios from "axios";
import MockAdapter from 'axios-mock-adapter';

export default function Index() {
    const [news, setNews] = useState([
        {
            id: 1,
            title: 'Lorem ipsum dolor sit amet',
            description: 'Consectetur adipiscing elit. Vivamus arcu libero, varius vitae diam in, sagittis facilisis augue',
            text: 'Duis velit metus, efficitur non orci vitae, dictum malesuada arcu. Quisque gravida lobortis maximus. ' +
                'Sed sodales nec lectus vitae volutpat. Donec id finibus nisi, et tempor arcu. Suspendisse eu sem pretium, auctor leo ut, efficitur mauris. ' +
                'Cras finibus ac sem sed eleifend. Nam vulputate mollis elit. In non blandit mi. Quisque nec diam ultricies, pharetra magna vitae, egestas enim.'
        },
        {
            id: 2,
            title: 'Integer mattis rutrum lectus, a dapibus risus',
            description: 'Donec facilisis dui neque, quis accumsan velit porta eleifend.',
            text: 'Morbi placerat posuere blandit. Donec non lacus quis quam finibus tincidunt vitae sed risus. Donec ut fringilla sapien. ' +
                'Proin et hendrerit elit, sit amet faucibus augue. Aliquam erat volutpat. In hac habitasse platea dictumst. ' +
                'Aliquam nec mauris et mi sagittis sollicitudin at accumsan odio. Cras felis massa, elementum at diam non, tincidunt interdum libero.'
        },
        {
            id: 3,
            title: 'Cras vehicula venenatis purus, sed hendrerit ex sodales vitae',
            description: 'Donec mollis, massa a facilisis semper, arcu mauris facilisis nisl, ut laoreet nulla leo quis lorem, etiam sagittis nisl leo, ac laoreet magna malesuada convallis.',
            text: 'Morbi risus ex, varius id cursus at, condimentum vitae velit. Duis consectetur magna sit amet dolor pharetra pellentesque. ' +
                'Cras viverra sodales cursus. Nam ut magna et erat lobortis vestibulum. Sed dictum dolor ut suscipit pulvinar. ' +
                'Phasellus hendrerit metus ac massa rhoncus, a sodales leo bibendum. Duis augue nisi, faucibus vel varius at, auctor sit amet purus.'
        }
    ]);

    const [addedArticle, setAddedArticle] = useState({title: '', description: '', text: ''});

    const [formModalOpen, setFormModalOpen] = useState(false);

    const handleFormFieldChange = (field, value) => {
        setAddedArticle({
            ...addedArticle,
            [field]: value
        })
    }

    const validateArticle = () => {
        return addedArticle.title.length >= 3 && addedArticle.description.length >= 3 && addedArticle.text.length >= 3;
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        if(validateArticle()) {
            const response = await axios.post('/news/create', addedArticle);

            if(response.data.status === 'ok') {
                console.log(response);

                let newArticleId = news[news.length-1].id + 1;

                console.log(news[news.length-1].id, newArticleId);

                setNews([...news,
                    {
                        id: newArticleId,
                        ...addedArticle
                    }
                ]);

                setFormModalOpen(false);
                setAddedArticle({title: '', description: '', text: ''});
            }
        }
    };

    const handleDelete = async (id) => {
        if(!id) {
            return;
        }

        const response = await axios.delete('/news/delete', id);

        if (response.data.status === 'ok') {
            console.log(response);

            setNews(news.filter(item => item.id !== id));
        }
    };

    const handleArticleEdit = async (article) => {
        if(!article || !article.id) {
            return;
        }

        const response = await axios.patch('/news/update', {
            article
        });

        if (response.data.status === 'ok') {
            console.log(response);

            const updatedNews = news.map(item => {
                if(item.id === article.id) {
                    return {
                        ...item,
                        title: article.title,
                        description: article.description,
                        text: article.text
                    }
                } else {
                    return item;
                }
            })

            setNews(updatedNews);
        }
    };

    const mock = new MockAdapter(axios);

    mock.onPost('/news/create').reply(200, {
        status: 'ok'
    });

    mock.onDelete('/news/delete').reply(200, {
        status: 'ok'
    })

    mock.onPatch('/news/update').reply(200, {
        status: 'ok'
    })

    return (
        <>
            <Header title={'News'} icon={'newspaper'} />
            <Divider />
            <Container textAlign={"left"}>
                <Modal
                    onClose={() => setFormModalOpen(false)}
                    onOpen={() => setFormModalOpen(true)}
                    open={formModalOpen}
                    trigger={<Button color={"green"}>Add</Button>   }
                >
                    <Modal.Header>
                        Add news
                    </Modal.Header>
                    <Modal.Content>
                        <NewsForm fields={addedArticle} changeArticle={handleFormFieldChange} />
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color='black' onClick={() => setFormModalOpen(false)}>
                            Cancel
                        </Button>
                        <Button
                            content="Add"
                            labelPosition='right'
                            icon='checkmark'
                            onClick={(e) => handleSubmit(e)}
                            color={"green"}
                        />
                    </Modal.Actions>
                </Modal>
                <Table headers={['#', 'Title', 'Description', 'Text']} rows={news} handleDelete={handleDelete} handleEdit={handleArticleEdit}/>
            </Container>
        </>
    );
}