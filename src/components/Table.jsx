import {Button, Icon, Table as SUTable, TextArea} from "semantic-ui-react";
import preciselySliceString from "../utils/preciselySliceString";
import {useEffect, useState} from "react";

export default function Table({headers, rows, handleDelete, handleEdit}) {
    const maxLength = 45;

    const [editedItem, setEditedItem] = useState({});

    const handleFieldChange = (field, value) => {
        setEditedItem({
            ...editedItem,
            [field]: value
        })
    }

    useEffect(
        () => setEditedItem({}), [rows]
    )

    return (
      <SUTable celled>
        <SUTable.Header>
          <SUTable.Row>
              {headers.map(header => (
                  <SUTable.HeaderCell>{header}</SUTable.HeaderCell>
              ))}
          </SUTable.Row>
        </SUTable.Header>
          <SUTable.Body>
              {rows.map((row, i) => (
                  <SUTable.Row key={row.id ? row.id : i}>
                      {Object.keys(row).map(val => (
                          <SUTable.Cell>
                              {editedItem && editedItem.id === row.id && val !== 'id' ?
                                  <TextArea
                                      style={{ minHeight: 50, minWidth: 200 }}
                                      value={editedItem[val]}
                                      name={val}
                                      onChange={(event) => handleFieldChange(event.target.name, event.target.value)}
                                  /> :
                                  <span>{preciselySliceString(maxLength, row[val])}</span>
                              }
                          </SUTable.Cell>
                      ))}
                      {
                          handleEdit &&
                          <SUTable.Cell>
                              {
                                  editedItem.id === row.id ?
                                      <Button title={"Apply"} icon color={"green"} onClick={() => handleEdit(editedItem)}>
                                          <Icon name='checkmark' />
                                      </Button> :
                                      <Button title={"Edit item"} icon color={"orange"} onClick={() => setEditedItem(row)}>
                                          <Icon name='edit' />
                                      </Button>
                              }
                          </SUTable.Cell>
                      }
                      {handleDelete &&
                          <SUTable.Cell>
                              <Button title={"Remove item"} icon color={"red"} onClick={() => handleDelete(row.id)}>
                                  <Icon name='delete' />
                              </Button>
                          </SUTable.Cell>
                      }
                  </SUTable.Row>
              ))}
          </SUTable.Body>
      </SUTable>
    );
}