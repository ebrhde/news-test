import {Menu as SUMenu} from "semantic-ui-react";

export default function Menu() {
    return (
        <SUMenu>
            <SUMenu.Item
                name='home'
                link
                onClick={() => {window.location.href="/"}}
            />
        </SUMenu>
    )
}