import {Form as SUForm} from "semantic-ui-react";

export default function Form({fields, changeArticle}) {
    return (
        <SUForm>
            {Object.keys(fields).map(field => (
                <SUForm.Field>
                    <label>{field.toUpperCase()} *</label>
                    <input
                        onChange={event => changeArticle(event.target.name, event.target.value)}
                        name={field}
                        value={fields[field]}
                        placeholder={'Some great ' + field + '...'}
                    />
                </SUForm.Field>
            ))}
        </SUForm>
    )
}