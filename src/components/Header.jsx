import {Container, Header as SemUiHeader, Icon} from "semantic-ui-react";

export default function Header({title, icon}) {
    return (
        <Container>
            <SemUiHeader as='h2' icon textAlign='center' color={"green"}>
                <Icon circular name={icon} />
                <SemUiHeader.Content>{title}</SemUiHeader.Content>
            </SemUiHeader>
        </Container>
    );
}